import os
import nox

# import nox_poetry.patch
from nox.sessions import Session

# Do not use anything installed in the site local directory (~/.local for example) which
# might have been installed by pip install --user.  These can prevent the install here
# from pulling in the correct packages, thereby mucking up tests later on.
# See https://stackoverflow.com/a/51640558/1088938
os.environ["PYTHONNOUSERSITE"] = "1"

args = dict(python=["3.8", "3.9", "3.10", "3.11", "3.12", "3.12"],
            reuse_venv=False)  # "2.7", "3.5",

# By default, we only execute the conda tests because the others required various python
# interpreters to be installed.  The other tests can be run, e.g., with `nox -s test` if
# desired.
nox.options.sessions = ["test"]

@nox.session(**args)
def test(session: Session) -> None:
    """Run the test suite."""
    print()
    session.run("poetry", "env", "use", session.virtualenv.interpreter, external=True)
    # session.run("poetry", "shell", external=True)
    session.run("poetry", "install", external=True)
    session.run("poetry", "run", "pytest", external=True)
    session.run("poetry", "run", "coverage", "xml", "--fail-under=0", external=True)
    #session.install(".")
    #session.run("pytest", external=True)
    #session.run("coverage", "xml", "--fail-under=0", external=True)


@nox.session(venv_backend="conda", **args)
def test_conda(session: Session):
    session.run("poetry", "env", "use", session.virtualenv.interpreter, external=True)
    # session.install(".")
    # session.run("poetry", "shell", external=True)
    session.run("poetry", "install", external=True)
    session.run("poetry", "run", "pytest", external=True)
    session.run("poetry", "run", "coverage", "xml", "--fail-under=0", external=True)
    
