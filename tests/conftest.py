"""Common test fixtures"""
import tempfile

import pytest


@pytest.fixture
def data_dir():
    with tempfile.TemporaryDirectory() as data_dir:
        yield data_dir
